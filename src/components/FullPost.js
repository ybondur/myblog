import React from "react";
import { Link } from "react-router-dom";

const FullPost = ({post, handleDeleteClick}) => {
    return (
        <div className="container">
            {
                post ? (
                    <div className="post">
                        <h4 className="center">{post.title}</h4>
                        <p>{post.body}</p>
                        <div className="row">
                            <span className="grey-text col s6 left-align">{post.author}</span>
                            <span className="grey-text col s6 right-align">{post.date}</span>
                        </div>
                        <div className="center">
                            <Link className="btn yellow" to={`/post/edit/${post.id}`}>Edit Post</Link>
                            <button className="btn red" onClick={handleDeleteClick}>Delete Post</button>
                        </div>
                    </div>
                ) : ( <div className="center">Loading post...</div> )
            }
        </div>
    );
};

export default FullPost;
