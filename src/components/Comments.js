import React from "react";

const Comments = ({comments}) => {
    return (
        <div className="commentsList">
            {
                comments ? (comments.map(comment =>  {
                    return(
                        <div className="position-card card" key={comment.id}>
                            <div className="card-content">
                                <p>{comment.body}</p>
                            </div>
                        </div>
                    );
                })) : ( <div></div> )
            }
        </div>
    );
};

export default Comments;
