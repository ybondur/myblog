import React from "react";
import { Link } from "react-router-dom";

const ShortPosts = ({posts}) => {
    return (
        <div className="postList">
            {
                posts.length ? (posts.map(post =>  {
                    return (
                        <div className="position-card post card" key={post.id}>
                            <div className="card-content">
                                <Link to={"/posts/" + post.id}>
                                <span className="card-title red-text">{post.title}</span>
                                </Link>
                                <p className="shortInfo">{post.body}</p>
                                <div className="row">
                                    <span className="grey-text col s6 left-align">{post.author}</span>
                                    <span className="grey-text col s6 right-align">{post.date}</span>
                                </div>
                            </div>
                        </div>
                    );
                })) : ( <div className="center">No posts to show</div> )
            }
        </div>
    );
};

export default ShortPosts;
