import React from "react";

const FormPost = ({author, title, body, handleInputChange, handleSubmit, handleReset}) => {
    return (
        <div className="row">
            <form onSubmit={ handleSubmit } className="col s12">
                <div className="row">
                    <div className="input-field col s6">
                        <input
                            id="author"
                            type="text"
                            className="form-control"
                            name="author"
                            placeholder="Author"
                            onChange={ handleInputChange }
                            value={ author }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s6">
                        <input
                            id="title"
                            type="text"
                            className="form-control"
                            name="title"
                            placeholder="Title"
                            onChange={ handleInputChange }
                            value={ title }
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s6">
                        <textarea
                            id="textarea"
                            cols="19"
                            rows="8"
                            className="materialize-textarea"
                            name="body"
                            placeholder="Body"
                            onChange={ handleInputChange }
                            value={ body }>
                        </textarea>
                    </div>
                </div>
                <div className="row">
                    <button type="submit" className="btn green col s3">Submit</button>
                    <button type="button" className="btn btn-warning col s3" onClick={ handleReset }>Reset</button>
                </div>
            </form>
        </div>
    );
};

export default FormPost;
