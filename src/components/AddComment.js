import React from "react";

const AddComment = ({body, handleInputChange, handleSubmit}) => {
    return (
        <form onSubmit={ handleSubmit } className="flex">
                <div className="input-field comment-form">
                    <textarea
                        id="textarea"
                        cols="19"
                        rows="8"
                        className="materialize-textarea"
                        name="body"
                        onChange={ handleInputChange }
                        value={ body }>
                    </textarea>
                    <label htmlFor="textarea">Comment</label>
                </div>
            <div className="comment-form">
                <button type="submit" className="btn green col s3">Comment</button>
            </div>
        </form>
    );
};

export default AddComment;
