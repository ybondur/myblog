import React from "react";
import { Link, NavLink, withRouter } from "react-router-dom";

const Navbar = (props) => {
    return (
        <nav className="nav-wrapper blue darken-3">
            <div className="container">
                <Link className="brand-logo" to="/">My blog</Link>
                <ul className="right">
                    <li><NavLink to="/post/new">Add Post</NavLink></li>
                </ul>
            </div>
        </nav>
    );
};

export default withRouter(Navbar);
