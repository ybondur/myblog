import React, { Component } from "react";
import Navbar from "./components/Navbar";
import { Route, BrowserRouter, Switch } from "react-router-dom";
import Home from "./containers/Home";
import Post from "./containers/Post";
import EditPost from "./containers/EditPost";
import NewPost from "./containers/NewPost";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/posts/:post_id" component={Post} />
            <Route path="/post/edit/:post_id" component={EditPost} />
            <Route path="/post/new" component={NewPost} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;