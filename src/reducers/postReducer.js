import { ADD_POST, DELETE_POST, FETCH_POST, EDIT_POST, ADD_COMMENT } from "../actions/types";

export default function postReducer(state = [], action) {
    switch (action.type) {
        case ADD_POST:
            return [...state, action.payload];
        case ADD_COMMENT:
            return state.map(post => {
                if (post.id === action.comment.postId) {
                    return Object.assign({}, post, {
                        comments: [...post.comments, action.comment]
                    });
                }
                return post;
            });
        case DELETE_POST:
            return state.filter(post => post.id !== action.payload.id);
        case EDIT_POST:
            return state.map(post => {
                if (post.id === action.payload.id) {
                    return Object.assign({}, post, {
                        author: action.payload.author,
                        title: action.payload.title,
                        body: action.payload.body,
                        date: action.payload.date
                    });
                }
                return post;
            });
        case FETCH_POST:
            return action.posts;
        default:
            return state;
    }
}
