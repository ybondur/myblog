import React, { Component } from "react";
import FormPost from "../components/FormPost";
import { connect } from "react-redux";
import { createPost } from "../actions/postActions";

class NewPost extends Component {
    state = {
        author: "",
        title: "",
        body: ""
    };

    handleInputChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        const d = new Date();
        const date = [d.getMonth()+1,
                      d.getDate(),
                      d.getFullYear()].join("/")+" "+
                      [d.getHours(),
                      d.getMinutes()].join(":");
        if (this.state.title.trim() && this.state.body.trim()) {
            this.props.onAddPost(Object.assign(this.state, {date: date}));
            this.handleReset();
            this.props.history.push("/");
        }
    };

    handleReset = () => {
        this.setState({
            author: "",
            title: "",
            body: ""
        });
    };

    render() {
        return (
            <div className="post-form">
                <FormPost
                    author={this.state.author}
                    body={this.state.body}
                    title={this.state.title}
                    handleInputChange={this.handleInputChange}
                    handleSubmit={this.handleSubmit}
                    handleReset={this.handleReset}
                />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddPost: post => {
            dispatch(createPost(post));
        }
    };
};

export default connect(null, mapDispatchToProps)(NewPost);
