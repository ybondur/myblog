import React, { Component } from "react"
import { connect } from "react-redux"
import { deletePost } from "../actions/postActions"
import Comments from "../components/Comments";
import FullPost from "../components/FullPost";
import AddComment from "../components/AddComment"
import { addComment } from "../actions/postActions";

class Post extends Component {

    state = {
        body: ""
    };

    handleReset = () => {
        this.setState({
            body: ""
        });
    };

    handleDeleteClick = () => {
        this.props.deletePost(this.props.post.id);
        this.props.history.push("/");
    };

    handleInputChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.addComment(Object.assign(this.state, { postId: this.props.post.id }))
        this.handleReset();
    };

    render() {
        const { post } = this.props;
        const comments = post ? post.comments : (null);
        return (
            <div>
                <FullPost post={post} handleDeleteClick={this.handleDeleteClick} />
                <Comments comments={comments} />
                <AddComment body={this.state.body} handleInputChange={this.handleInputChange} handleSubmit={this.handleSubmit}/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = parseInt(ownProps.match.params.post_id);
    return {
        post: state.posts.find(post => post.id === id)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deletePost: (id) => dispatch(deletePost(id)),
        addComment: (comment) => dispatch(addComment(comment))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)