import React, { Component } from "react";
import { connect } from "react-redux";
import ShortPosts from "../components/ShortPosts";

class Home extends Component {
    render() {
        const { posts } = this.props;
        return (
            <div>
                <ShortPosts posts={posts} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state.posts
    };
};

export default connect(mapStateToProps)(Home);
