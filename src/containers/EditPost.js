import React, { Component } from "react"
import FormPost from "../components/FormPost"
import { connect } from "react-redux";
import { editPost } from "../actions/postActions";

class EditPost extends Component {
    state = {
        author: this.props.post.author,
        title: this.props.post.title,
        body: this.props.post.body
    };

    handleInputChange = e => {
        this.setState({
        [e.target.name]: e.target.value
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        const d = new Date();
        const date = [d.getMonth()+1,
                d.getDate(),
                d.getFullYear()].join("/")+" "+
                [d.getHours(),
                d.getMinutes()].join(":");
        if (this.state.title.trim() && this.state.body.trim()) {
            this.props.onEditPost(Object.assign(this.state, {date: date}, {id: this.props.post.id}));
            this.props.history.push(`/posts/${this.props.post.id}`);
        }
    };

    handleReset = () => {
        this.setState({
            author: "",
            title: "",
            body: ""
        });
    };

    render() {
        return (
            <div className="post-form">
                <FormPost
                    author={this.state.author}
                    body={this.state.body}
                    title={this.state.title}
                    handleInputChange={this.handleInputChange}
                    handleSubmit={this.handleSubmit}
                    handleReset={this.handleReset}
                />
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = parseInt(ownProps.match.params.post_id);
    return {
        post: state.posts.find(post => post.id === id)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onEditPost: post => {
            dispatch(editPost(post));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPost);
