import { ADD_POST, DELETE_POST, FETCH_POST, EDIT_POST, ADD_COMMENT } from "./types";
import axios from "axios";

const apiUrl = "https://simple-blog-api.crew.red/posts";

export const createPost = ({ title, body, author, date }) => {
    return (dispatch) => {
        return axios.post(`${apiUrl}`, { title, body, author, date })
        .then(response => {
            dispatch(createPostSuccess(response.data));
        })
        .catch(error => {
            throw(error);
        });
    };
};

export const createPostSuccess =  (data) => {
    return {
        type: ADD_POST,
        payload: {
        id: data.id,
        title: data.title,
        body: data.body,
        author: data.author,
        date: data.date
        }
    };
};

export const editPost = ({ id, title, body, author, date }) => {
    return (dispatch) => {
        return axios.put(`${apiUrl}/${id}`, { title, body, author, date })
            .then(response => {
                dispatch(editPostSuccess(response.data));
            })
            .catch(error => {
                throw(error);
            });
    };
};

  export const editPostSuccess = (data) => {
    return {
        type: EDIT_POST,
        payload: {
            id: data.id,
            title: data.title,
            body: data.body,
            author: data.author,
            date: data.date
        }
    };
};

export const deletePostSuccess = id => {
    return {
        type: DELETE_POST,
        payload: {
            id
        }
    };
};

export const deletePost = id => {
return (dispatch) => {
    return axios.delete(`${apiUrl}/${id}`)
        .then(response => {
            dispatch(deletePostSuccess(id));
        })
        .catch(error => {
            throw(error);
        });
    };
};

export const fetchPosts = (posts) => {
    return {
        type: FETCH_POST,
        posts
    };
};

export const fetchAllPosts = () => {
    return (dispatch) => {
        return axios.get(`${apiUrl}?_embed=comments`)
        .then(response => {
            dispatch(fetchPosts(response.data));
        })
        .catch(error => {
            throw(error);
        });
    };
};

export const addCommentSucces = (comment) => {
    return {
        type: ADD_COMMENT,
        comment
    };
};

  export const addComment = ({ postId, body }) => {
    return (dispatch) => {
        return axios.post("https://simple-blog-api.crew.red/comments", { postId, body })
        .then(response => {
            dispatch(addCommentSucces(response.data));
        })
        .catch(error => {
            throw(error);
        });
    };
};
